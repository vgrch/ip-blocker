1.Get ip range by country from : https://lite.ip2location.com/azerbaijan-ip-address-ranges


2.Generate range list like (iprange.txt) : 


5.10.240.0-5.10.255.255


5.44.32.0-5.44.39.255


....



3. Calculate cidr notation for each range :


$ awk '{system("ipcalc -rn "$1 "| tail -n +2")}' iprange.txt > calculated-cidr.txt


4. Generate firewalld rich rules:


$ ./rule-generator.sh calculated-cidr.txt


5. Apply new rules : 


$ ./firewalld-rules.sh


6. firewall-cmd --reload
