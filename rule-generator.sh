while IFS='' read -r line || [[ -n "$line" ]]; do
echo "firewall-cmd --permanent --zone=public --add-rich-rule='rule family="\""ipv4"\"" source address="\""$line"\"" port protocol="\""tcp"\"" port="\""25"\"" accept'" >> firewalld-rules.txt;
done < "$1"
